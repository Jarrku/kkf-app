import React from 'react';
import { AppRegistry } from 'react-native';

import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import ApolloProvider from './ApolloProvider';

function Root() {
  return (
    <ApolloProvider><App /></ApolloProvider>
  )
}


AppRegistry.registerComponent('Root', () => Root);
AppRegistry.runApplication('Root', { rootTag: document.getElementById('root') })

serviceWorker.register();
