import React, { Component } from "react";
import { } from 'react-native';
import idx from "idx";

import logo from "./logo.svg";
import "./App.css";

import gql from "graphql-tag";
import { Query, Subscription } from "react-apollo";
import { GetUsers, Test } from "./types";

const GET_POSTS = gql`
  query GetUsers {
    orders {
      id
      name
    }
  }
`;

const USER_SUB = gql`
  subscription Test {
    order {
      node {
        name
      }
    }
  }
`;

class UserQuery extends Query<GetUsers> {}

class UserSub extends Subscription<Test> {}

function TestPage() {
  return (
    <header className="App-header">
      <img src={logo} className="App-logo" alt="logo" />
      <p>
        Edit <code>src/App.js</code> and save to reload.
      </p>
      <a className="App-link" href="https://reactjs.org" target="_blank" rel="noopener noreferrer">
        Learn React
      </a>
      <UserQuery query={GET_POSTS}>
        {({ loading, error, data }) => {
          if (loading || !data) return <div>Loading...</div>;
          if (error) return <div>Error :(</div>;

          return (
            <ul>
              {data.orders.map(order => {
                if (!order) return null;
                return (
                  <li key={order.id}>
                    {order.name} - {order.id}
                  </li>
                );
              })}
            </ul>
          );
        }}
      </UserQuery>
      <UserSub subscription={USER_SUB}>
        {({ data, loading }) => {
          const name = idx(data, _ => _.order.node.name);

          return <h4>New order: {!loading && name}</h4>;
        }}
      </UserSub>
    </header>
  );
}

export default TestPage;
