
import { action, observable, computed } from "mobx";
import { Category, GetItems_items } from "../types";

class OrderForm {
  @observable name = "";
  @observable remark = "";
  readonly items = observable<FormItem>([])
  @observable hasBeenSeeded = false;

  @computed get total() {
    return this.items.reduce((total, item) => {
      return total + item.amount * item.price;
    }, 0);
  }

  @computed get valid() {
    return Boolean(this.name) && !this.items.every(item => item.amount === 0);
  }

  @computed get amountOfMenus() {
    return this.items.reduce((menus, item) => {
      const shouldAdd = item.category === Category.ADULT || item.category === Category.CHILD;
      if (!shouldAdd) return menus;;

      return menus + item.amount;
    }, 0)
  }

  @computed get soupAmount() {
    const soup = this.items.find(item => item.name.toLowerCase() === 'soep');
    return soup ? soup.amount : 0;
  }

  @computed get adultItems() {
    return this.items.filter(item => item.category === Category.ADULT);
  }

  @computed get childItems() {
    return this.items.filter(item => item.category === Category.CHILD);
  }

  @computed get discountCardAmount() {
    const result = this.items.find(item => item.category === Category.DISCOUNT);
    return result ? result.amount : 0;
  }

  @computed get CreateOrderData() {
    const order = this.items.reduce<Array<{ id: string }>>((result, item) => {
      for (let i = 0; i < item.amount; i++) {
        result.push({ id: item.id });
      }
      return result;
    }, []);

    return {
      data: {
        name: this.name,
        remark: this.remark,
        order: {
          connect: order,
        }
      }
    }
  }

  @action.bound
  setName(name: string) {
    this.name = name;
  }

  @action.bound
  setRemark(remark: string) {
    this.remark = remark;
  }

  @action.bound
  reset() {
    this.name = "";
    this.remark = "";
    this.items.forEach(item => item.reset());
  }

  @action.bound
  seedMenuData(menuItems: GetItems_items[]) {
    if (!this.hasBeenSeeded) {
      const items = menuItems.map(item => new FormItem(item));
      this.items.replace(items);
      this.hasBeenSeeded = true;
    }
  }
}

const form = new OrderForm();

export default form;

class FormItem {
  constructor(menuItem: GetItems_items) {
    this.id = menuItem.id;
    this.category = menuItem.category;
    this.name = menuItem.name;
    this.price = menuItem.price;
  }

  id = "";

  @observable category?: Category;
  @observable name = "";
  @observable price = 0;
  @observable amount = 0;

  @action.bound
  setAmount(value: number) {
    this.amount = value;
  }

  @action.bound
  reset() {
    this.amount = 0;
  }
}