import { action, observable, computed } from "mobx";
import { Category, OrderState, GetOrders_orders } from "../types";
import { getDay } from "date-fns";
import { totalmem } from "os";


class Kitchen {
  readonly orders = observable<Order>([])

  @computed
  get nonScannedOrders() {
    return this.orders.filter(order => order.state === OrderState.ORDERED);
  }

  @computed
  get pendingOrders() {
    return this.orders.filter(order => order.state === OrderState.SOUP_DELIVERED || order.state === OrderState.SCANNED);
  }

  @computed
  get amountOfChicken() {
    return this.pendingOrders.reduce((total, order) => total + order.amountOfChicken, 0);
  }

  @action.bound
  seedOrders(orders: GetOrders_orders[]) {
    const currentDay = getDay(Date.now());

    const onlyToday = orders.filter(o => {
      const orderDay = getDay(o.createdAt);
      return orderDay === currentDay;
    })

    this.orders.replace(onlyToday.map(o => new Order(o)));
  }

  @action.bound
  updateOrder(newOrder: GetOrders_orders) {
    const foundOrder = this.orders.find(order => order.id === newOrder.id);

    if (foundOrder) {
      foundOrder.updateOrder(newOrder)
    } else {
      const order = new Order(newOrder);
      this.orders.push(order);
    }
  }
}

const kitchenStore = new Kitchen();
export default kitchenStore;
// @ts-ignore
window.kitchenStore = kitchenStore;

export class Order {
  constructor(order: GetOrders_orders) {
    this.id = order.id;
    this.updateOrder(order);
  }

  readonly id: string;
  @observable name = "";
  @observable remark = "";
  @observable state: OrderState = OrderState.ORDERED;
  @observable tableNo: string | null = null;
  @observable createdAt = "";
  @observable updatedAt = ""
  @observable scannedAt: string | null = null;
  readonly items = observable<OrderItem>([])

  @computed
  get amountOfChicken() {
    return this.items.reduce((total, item) => total + item.amountOfChicken, 0);
  }

  @computed
  get hasSoup() {
    const index = this.items.findIndex(item => item.name === 'Soep');
    return index !== -1;
  }

  @computed
  get soep() {
    return this.items.filter(item => item.name === 'Soep').length;
  }

  @computed
  get natuurVolw() {
    return this.items.filter(item => item.name === 'Kip Natuur' && item.category === Category.ADULT).length;
  }

  @computed
  get curryVolw() {
    return this.items.filter(item => item.name === 'Kip Curry' && item.category === Category.ADULT).length;
  }

  @computed
  get provencalVolw() {
    return this.items.filter(item => item.name === 'Kip Provencal' && item.category === Category.ADULT).length;
  }

  @computed
  get appelmoesVolw() {
    return this.items.filter(item => item.name === 'Kip Appelmoes' && item.category === Category.ADULT).length;
  }

  @computed
  get natuurKind() {
    return this.items.filter(item => item.name === 'Kip Natuur' && item.category === Category.CHILD).length;
  }

  @computed
  get curryKind() {
    return this.items.filter(item => item.name === 'Kip Curry' && item.category === Category.CHILD).length;
  }

  @computed
  get provencalKind() {
    return this.items.filter(item => item.name === 'Kip Provencal' && item.category === Category.CHILD).length;
  }

  @computed
  get appelmoesKind() {
    return this.items.filter(item => item.name === 'Kip Appelmoes' && item.category === Category.CHILD).length;
  }

  @computed
  get total() {
    return this.soep + this.natuurVolw + this.curryVolw + this.provencalVolw + this.appelmoesVolw + this.natuurKind + this.curryKind + this.provencalKind + this.appelmoesKind
  }

  @computed
  get nextState() {
    if (this.state === OrderState.ORDERED) {
      return OrderState.SCANNED;
    }

    if (this.state === OrderState.SOUP_DELIVERED) {
      return OrderState.COMPLETED;
    }

    if (this.state === OrderState.SCANNED) {
      return this.hasSoup ? OrderState.SOUP_DELIVERED : OrderState.COMPLETED;
    }

    return OrderState.COMPLETED;
  }

  @computed
  get actionName() {
    if(this.nextState === OrderState.SOUP_DELIVERED) {
      return "Soep gaat buiten"
    }
    if(this.nextState === OrderState.SCANNED) {
      return "Bestelling komt binnen";
    }

    return "Bestelling gaat buiten";
  }

  @action.bound
  updateOrder(order: GetOrders_orders) {
    this.name = order.name;
    this.remark = order.remark;
    this.state = order.state;
    this.tableNo = order.tableNo;
    this.createdAt = order.createdAt;
    this.scannedAt = order.scannedAt;
    this.updatedAt = order.updatedAt;

    if (order.order) {
      this.items.replace(order.order.map(o => new OrderItem(o)));
    }
  }
}

class OrderItem {
  constructor(item: { category: Category, name: string }) {
    this.category = item.category;
    this.name = item.name;
  }

  @observable category?: Category;
  @observable name = "";

  @computed
  get amountOfChicken() {
    if (this.category === Category.ADULT) return 0.5;
    if (this.category === Category.CHILD) return 0.25;
    return 0;
  }
}