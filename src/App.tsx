import React from 'react';
import { StyleSheet, View } from 'react-native';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import Entry from './pages/Entry';
import TestPage from './TestPage';
import Navigation from './components/Navigation';

import './App.css';
import ScanPage from './pages/ScanPage';
import Kitchen from './pages/KitchenPage';

function App() {
  return (
    <BrowserRouter>
      <View style={styles.box}>
        <Navigation />
        <hr />
        <Switch>
          <Route exact path="/" component={Entry} />
          <Route path="/test" component={TestPage} />
          <Route path="/scan/:id" component={ScanPage} />
          <Route path="/scan" component={ScanPage} />
          <Route path="/kitchen" component={Kitchen} />
        </Switch>
      </View>
    </BrowserRouter>
  );
}


const styles = StyleSheet.create({
  box: { padding: 10 },
});


export default App;
