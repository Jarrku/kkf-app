/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: CreateItem
// ====================================================

export interface CreateItem_createItem {
  __typename: "Item";
  name: string;
}

export interface CreateItem {
  createItem: CreateItem_createItem;
}

export interface CreateItemVariables {
  data: ItemCreateInput;
}

/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GetItems
// ====================================================

export interface GetItems_items {
  __typename: "Item";
  id: string;
  name: string;
  price: number;
  category: Category;
}

export interface GetItems {
  items: (GetItems_items | null)[];
}

/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: CreateOrder
// ====================================================

export interface CreateOrder_createOrder {
  __typename: "Order";
  id: string;
}

export interface CreateOrder {
  createOrder: CreateOrder_createOrder;
}

export interface CreateOrderVariables {
  data: OrderCreateInput;
}

/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GetOrders
// ====================================================

export interface GetOrders_orders_order {
  __typename: "Item";
  name: string;
  category: Category;
}

export interface GetOrders_orders {
  __typename: "Order";
  id: string;
  name: string;
  remark: string;
  state: OrderState;
  tableNo: string | null;
  order: GetOrders_orders_order[] | null;
  updatedAt: any;
  scannedAt: any | null;
  createdAt: any;
}

export interface GetOrders {
  orders: (GetOrders_orders | null)[];
}

/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL subscription operation: OrderSubscription
// ====================================================

export interface OrderSubscription_order_node_order {
  __typename: "Item";
  name: string;
  category: Category;
}

export interface OrderSubscription_order_node {
  __typename: "Order";
  id: string;
  name: string;
  remark: string;
  state: OrderState;
  tableNo: string | null;
  order: OrderSubscription_order_node_order[] | null;
  updatedAt: any;
  scannedAt: any | null;
  createdAt: any;
}

export interface OrderSubscription_order {
  __typename: "OrderSubscriptionPayload";
  node: OrderSubscription_order_node | null;
}

export interface OrderSubscription {
  order: OrderSubscription_order | null;
}

/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: UpdateOrder
// ====================================================

export interface UpdateOrder_updateOrder {
  __typename: "Order";
  id: string;
  name: string;
}

export interface UpdateOrder {
  updateOrder: UpdateOrder_updateOrder | null;
}

export interface UpdateOrderVariables {
  data: OrderUpdateInput;
  where: OrderWhereUniqueInput;
}

/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GetUsers
// ====================================================

export interface GetUsers_orders {
  __typename: "Order";
  id: string;
  name: string;
}

export interface GetUsers {
  orders: (GetUsers_orders | null)[];
}

/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL subscription operation: Test
// ====================================================

export interface Test_order_node {
  __typename: "Order";
  name: string;
}

export interface Test_order {
  __typename: "OrderSubscriptionPayload";
  node: Test_order_node | null;
}

export interface Test {
  order: Test_order | null;
}

/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

//==============================================================
// START Enums and Input Objects
//==============================================================

export enum Category {
  ADULT = "ADULT",
  CHILD = "CHILD",
  DISCOUNT = "DISCOUNT",
  EXTRAS = "EXTRAS",
}

export enum OrderState {
  COMPLETED = "COMPLETED",
  ORDERED = "ORDERED",
  SCANNED = "SCANNED",
  SOUP_DELIVERED = "SOUP_DELIVERED",
}

export interface ItemCreateInput {
  name: string;
  price: number;
  category: Category;
}

export interface ItemCreateManyInput {
  create?: ItemCreateInput[] | null;
  connect?: ItemWhereUniqueInput[] | null;
}

export interface ItemScalarWhereInput {
  AND?: ItemScalarWhereInput[] | null;
  OR?: ItemScalarWhereInput[] | null;
  NOT?: ItemScalarWhereInput[] | null;
  id?: string | null;
  id_not?: string | null;
  id_in?: string[] | null;
  id_not_in?: string[] | null;
  id_lt?: string | null;
  id_lte?: string | null;
  id_gt?: string | null;
  id_gte?: string | null;
  id_contains?: string | null;
  id_not_contains?: string | null;
  id_starts_with?: string | null;
  id_not_starts_with?: string | null;
  id_ends_with?: string | null;
  id_not_ends_with?: string | null;
  name?: string | null;
  name_not?: string | null;
  name_in?: string[] | null;
  name_not_in?: string[] | null;
  name_lt?: string | null;
  name_lte?: string | null;
  name_gt?: string | null;
  name_gte?: string | null;
  name_contains?: string | null;
  name_not_contains?: string | null;
  name_starts_with?: string | null;
  name_not_starts_with?: string | null;
  name_ends_with?: string | null;
  name_not_ends_with?: string | null;
  price?: number | null;
  price_not?: number | null;
  price_in?: number[] | null;
  price_not_in?: number[] | null;
  price_lt?: number | null;
  price_lte?: number | null;
  price_gt?: number | null;
  price_gte?: number | null;
  category?: Category | null;
  category_not?: Category | null;
  category_in?: Category[] | null;
  category_not_in?: Category[] | null;
}

export interface ItemUpdateDataInput {
  name?: string | null;
  price?: number | null;
  category?: Category | null;
}

export interface ItemUpdateManyDataInput {
  name?: string | null;
  price?: number | null;
  category?: Category | null;
}

export interface ItemUpdateManyInput {
  create?: ItemCreateInput[] | null;
  connect?: ItemWhereUniqueInput[] | null;
  set?: ItemWhereUniqueInput[] | null;
  disconnect?: ItemWhereUniqueInput[] | null;
  delete?: ItemWhereUniqueInput[] | null;
  update?: ItemUpdateWithWhereUniqueNestedInput[] | null;
  updateMany?: ItemUpdateManyWithWhereNestedInput[] | null;
  deleteMany?: ItemScalarWhereInput[] | null;
  upsert?: ItemUpsertWithWhereUniqueNestedInput[] | null;
}

export interface ItemUpdateManyWithWhereNestedInput {
  where: ItemScalarWhereInput;
  data: ItemUpdateManyDataInput;
}

export interface ItemUpdateWithWhereUniqueNestedInput {
  where: ItemWhereUniqueInput;
  data: ItemUpdateDataInput;
}

export interface ItemUpsertWithWhereUniqueNestedInput {
  where: ItemWhereUniqueInput;
  update: ItemUpdateDataInput;
  create: ItemCreateInput;
}

export interface ItemWhereUniqueInput {
  id?: string | null;
}

export interface OrderCreateInput {
  name: string;
  remark: string;
  state?: OrderState | null;
  tableNo?: string | null;
  scannedAt?: any | null;
  deliveredAt?: any | null;
  order?: ItemCreateManyInput | null;
}

export interface OrderUpdateInput {
  name?: string | null;
  remark?: string | null;
  state?: OrderState | null;
  tableNo?: string | null;
  scannedAt?: any | null;
  deliveredAt?: any | null;
  order?: ItemUpdateManyInput | null;
}

export interface OrderWhereUniqueInput {
  id?: string | null;
}

//==============================================================
// END Enums and Input Objects
//==============================================================
