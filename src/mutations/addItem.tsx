import gql from 'graphql-tag';
import React from 'react';
import { useMutation } from 'react-apollo-hooks';

import { CreateItem, CreateItemVariables, Category } from '../types';

const CREATE_ITEM = gql`
  mutation CreateItem($data: ItemCreateInput!) {
    createItem(data: $data){
      name
    }
  }
`

const NewItemForm = () => {
  const [name, setName] = React.useState('');
  const [price, setPrice] = React.useState(2.5);
  const [category, setCategory] = React.useState<Category>(Category.EXTRAS);

  const addItem = useMutation<CreateItem, CreateItemVariables>(CREATE_ITEM, {
    variables: {
      data: {
        name,
        price,
        category,
      }
    }
  });

  return (
    <div>
      <input value={name} onChange={(event) => { setName(event.currentTarget.value)}} />
      <button onClick={() => addItem()}>Add Item!</button>
    </div>
  )
}

export default NewItemForm;