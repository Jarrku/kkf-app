import * as React from 'react';
import { View, Button, Text, StyleSheet, TextInput } from 'react-native';

interface Props {
  label: string;
  value: number;
  setValue: (value: number) => any;
  max?: number
  min?: number
}

function NumberInput({ label, value = 0, setValue, min, max }: Props) {
  function onIncrement() {
    setValue(value + 1);
  }

  function onDecrement() {
    setValue(value - 1);
  }

  function onChange(value: string) {
    if (value === "") return setValue(0);

    const newValue = Number.parseInt(value, 10);
    if (!isNaN(newValue)) setValue(newValue);
  }

  const inputValue = value.toString();

  const disableDecrement = typeof min !== 'undefined' && value <= min;
  const disableIncrement = typeof max !== 'undefined' && value >= max;

  return (
    <View style={styles.container}>
      <Text style={styles.label}>{label}</Text>
      <View style={styles.button}>
        <Button title="-" onPress={onDecrement} disabled={disableDecrement} />
      </View>
      <TextInput style={styles.input} keyboardType="numbers-and-punctuation" value={inputValue} onChangeText={onChange}></TextInput>
      <View style={styles.button}>
        <Button title="+" onPress={onIncrement} disabled={disableIncrement} />
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    padding: 5,
    display: 'flex',
    flexGrow: 1,
    flexShrink: 0,
    flexBasis: 'auto',
    minWidth: '50%',
    flexDirection: 'row',
    alignItems: "center",
  },
  label: {
    flexGrow: 0,
    flexShrink: 0,
    flexBasis: 110,
  },
  input: {
    width: 60,
    paddingTop: 10,
    paddingBottom: 10,
    textAlign: 'center',
  },
  button: {
    flexGrow: 0,
    flexShrink: 0,
    flexBasis: 40,
    marginLeft: 10,
    marginRight: 10,
    justifyContent: 'center',
  }
})

export default NumberInput;