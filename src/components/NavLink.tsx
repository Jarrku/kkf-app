import React from 'react'
import { Button } from 'react-native'
import { withRouter, RouteComponentProps } from 'react-router-dom'

interface Props extends RouteComponentProps {
  to: string;
  children: string
}

function NavLink(props: Props) {
  return (
    <Button title={props.children} onPress={() => props.history.push(props.to)}></Button>
  )
}

export default withRouter(NavLink);
