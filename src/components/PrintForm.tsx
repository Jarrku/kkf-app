import * as React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { observer } from 'mobx-react-lite';
import QRCode from 'qrcode.react';
import { format } from 'date-fns';

import form from '../models/OrderForm';

interface Props {
  id: string;
}

const PrintForm = observer(({ id }: Props) => {
  const currentDate = format(new Date(), "DD-MM-YY HH:MM:SS");
  const url = `http://${process.env.REACT_APP_API_URL}:${process.env.REACT_APP_API_URL_PORT}/scan/${id}`;

  return (
    <View style={styles.container} testID="print">
      <View style={styles.header}>
        <Text style={styles.text}>Naam: {form.name}</Text>
        <Text style={styles.text}>Besteld: {currentDate}</Text>
      </View>
      <View style={styles.qrContainer}>
        <Text style={styles.soupText}>Soep</Text>
        <Text style={styles.soupText}>{form.soupAmount}</Text>
        <QRCode value={url} size={160} />
      </View>
      <View style={styles.menu}>
        <View style={styles.menuRow}>
          <Text style={styles.menuCategory}>Volwassene</Text>
          {form.adultItems.map(item => (
            <ItemRow key={item.name} label={item.name} amount={item.amount} />
          ))}
        </View>
        <View style={styles.menuRow}>
          <Text style={styles.menuCategory}>Kinderen</Text>
          {form.childItems.map(item => (
            <ItemRow key={item.name} label={item.name} amount={item.amount} />
          ))}
        </View>
      </View>
      {Boolean(form.remark) && (
        <View style={styles.tableNoContainer}>
          <Text style={styles.text}>{form.remark}</Text>
        </View>
      )}
      <View style={styles.tableNoContainer}>
        <Text style={styles.soupText}>Tafelnummer: </Text>
        <View style={styles.inputField}></View>
      </View>
      <View style={styles.overview}>
        <View style={styles.menuRow}>
          <View style={styles.menuItemRow}>
            <View style={styles.menuItemContainer}><Text style={styles.menuItemLabel}>Kaarten</Text></View>
            <View style={styles.menuItemContainer}><Text style={styles.menuItemAmount}>{form.discountCardAmount}</Text></View>
          </View>
          <View style={styles.menuItemRow}>
            <View style={styles.menuItemContainer}><Text style={styles.menuItemLabel}>Totaal</Text></View>
            <View style={styles.menuItemContainer}><Text style={styles.menuItemAmount}>&euro; {form.total}</Text></View>
          </View>
        </View>
        <View style={styles.menuRow}>
          <View style={styles.totalContainer}>
            <Text style={styles.menuItemAmount}>Aantal menu's</Text>
            <Text style={styles.totalNumberText}>{form.amountOfMenus}</Text>
          </View>
        </View>
      </View>
    </View>
  );
});

interface ItemRowProps {
  label: string;
  amount: number;
}

const ItemRow = ({ label, amount }: ItemRowProps) => (
  <View style={styles.menuItemRow}>
    <View style={styles.menuItemContainer}><Text style={styles.menuItemLabel}>{label}</Text></View>
    <View style={styles.menuItemContainer}><Text style={styles.menuItemAmount}>{amount}</Text></View>
  </View>
);

const containerStyles = {
  paddingBottom: 30,
  paddingTop: 30,
  borderBottomColor: "black",
  borderBottomStyle: "solid",
  borderBottomWidth: 1,
}

const fontStyles = {
  fontSize: 26,
}

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    height: '90vh',
    paddingLeft: 60,
    paddingRight: 60,
    paddingTop: 20,
    paddingBottom: 20,
  },
  text: {
    ...fontStyles,
  },
  header: {
    display: 'flex',
    justifyContent: 'space-between',
    flexDirection: 'row',
    ...containerStyles,
  },
  qrContainer: {
    display: 'flex',
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    ...containerStyles,
  },
  soupText: {
    fontSize: 32,
    fontWeight: 'bold',
  },
  menu: {
    display: 'flex',
    flexDirection: 'row',
    ...containerStyles,
  },
  menuRow: {
    flex: 1,
    flexBasis: 50,
  },
  menuCategory: {
    fontSize: 32,
    fontWeight: 'bold',
    paddingBottom: 10,
  },
  menuItemRow: {
    display: 'flex',
    flexDirection: 'row',
  },
  menuItemContainer: {
    flex: 1,
    flexBasis: 50,
    paddingTop: 30,
  },
  menuItemLabel: {
    ...fontStyles,
    fontWeight: 'bold',
  },
  menuItemAmount: {
    ...fontStyles,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  tableNoContainer: {
    ...containerStyles,
    display: 'flex',
    flexDirection: 'row',
  },
  inputField: {
    flex: 1,
    marginLeft: 20,
    borderBottomColor: "lightgrey",
    borderBottomStyle: "dashed",
    borderBottomWidth: 1,
  },
  overview: {
    display: 'flex',
    flexDirection: 'row',
    ...containerStyles,
    borderBottomWidth: 0,
  },
  totalContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    borderColor: "black",
    borderStyle: "dashed",
    borderWidth: 1,
  },
  totalNumberText: {
    fontSize: 32,
    fontWeight: 'bold',
    paddingTop: 10,
  }
});

export default PrintForm;