import React from 'react';
import { StyleSheet, View, Text } from 'react-native';

import NavLink from './NavLink';

const links = [
  {
    to: '/',
    label: 'Home',
  },
  // {
  //   to: '/test',
  //   label: 'Test',
  // },
  {
    to: '/kitchen',
    label: 'Keuken',
  }
]

export default function Navigation() {
  return (
    <View style={styles.list} testID="no-print">
      <Text style={styles.titleText}>Kiekenfeesten</Text>
      {links.map(link => (
        <View key={link.label} style={styles.item}><NavLink to={link.to}>{link.label}</NavLink></View>
      ))}
    </View>
  )
}

const styles = StyleSheet.create({
  list: {
    display: "flex",
    flexDirection: 'row',
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "lightgrey",
  },
  titleText: {
    fontSize: 20,
  },
  item: {
    margin: 10,
  }
})