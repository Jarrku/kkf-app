import * as React from 'react';
import { observer } from 'mobx-react-lite';
import { Order } from '../models/Kitchen';
import { View, Text, Button } from 'react-native';
import { MutationFn } from 'react-apollo-hooks';
import { UpdateOrder, UpdateOrderVariables, OrderState } from '../types';
import { format } from 'date-fns';


interface Props {
  order: Order
  updateOrder: MutationFn<UpdateOrder, UpdateOrderVariables>
}

const classNames = {
  [OrderState.SCANNED]: 'white',
  [OrderState.SOUP_DELIVERED]: 'grey',
  [OrderState.]: 'lightgrey',

}

const KitchenRow = observer(({ order, updateOrder }: Props) => {

  const onPress = React.useCallback(() => {
    updateOrder({
      variables: {
        data: {
          state: order.nextState,
        },
        where: {
          id: order.id,
        }
      },
    });
  }, [order.nextState, order.id, updateOrder]);

  const scannedAt = order.scannedAt ? format(order.scannedAt, 'HH:MM:SS') : '';

  const className = order.stat

  return (
    <div >
      <span>{order.name}</span>
      <span>{scannedAt}</span>
      <span>{order.soep}</span>
      <span>{order.natuurVolw}</span>
      <span>{order.curryVolw}</span>
      <span>{order.provencalVolw}</span>
      <span>{order.appelmoesVolw}</span>
      <span>{order.natuurKind}</span>
      <span>{order.curryKind}</span>
      <span>{order.provencalKind}</span>
      <span>{order.appelmoesKind}</span>
      <span>{order.total}</span>
      <span>{order.tableNo}</span>
      <Button onPress={onPress} title={order.actionName}></Button>
    </div>
  )
});

export default KitchenRow;