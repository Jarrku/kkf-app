import * as React from 'react';
import { observer } from 'mobx-react-lite';
import { Order } from '../models/Kitchen';
import { View, Text, Button } from 'react-native';
import { MutationFn } from 'react-apollo-hooks';
import { UpdateOrder, UpdateOrderVariables } from '../types';


interface Props {
  order: Order
  updateOrder: MutationFn<UpdateOrder, UpdateOrderVariables>
}

const KitchenRow = observer(({ order, updateOrder }: Props) => {

  const onPress = React.useCallback(() => {
    updateOrder({
      variables: {
        data: {
          state: order.nextState,
        },
        where: {
          id: order.id,
        }
      },
    });
  }, [order.nextState, order.id, updateOrder]);

  return (
    <View style={{ display: 'flex', flexDirection: "row", padding: 10, }}>
      <Text>Soup: {order.hasSoup}</Text>
      <Text>Tafel: {order.tableNo}</Text>
      <Text>Staat: {order.state}</Text>
      <Text>Naam: {order.name}</Text>
      <Button onPress={onPress} title="Go To Next State" />
    </View>
  )
});

export default KitchenRow;