import * as React from "react";
import { useQuery, useMutation } from 'react-apollo-hooks';
import gql from "graphql-tag";
import { View, Text, Button, StyleSheet, TextInput } from "react-native";
import { observer } from "mobx-react-lite";

import { GetItems, GetItems_items, CreateOrder, CreateOrderVariables } from '../types';
import NumberInput from "../components/NumberInput";
import PrintForm from "../components/PrintForm";
import form from "../models/OrderForm";

const GET_ITEMS = gql`
  query GetItems {
    items {
      id
      name
      price
      category
    }
  }
`

const CREATE_ORDER = gql`
  mutation CreateOrder($data: OrderCreateInput!) {
    createOrder(data: $data) {
      id
    }
  }
`

const Entry = observer(() => {
  const [orderId, setOrderId] = React.useState('');
  const { data, error } = useQuery<GetItems>(GET_ITEMS)

  const createOrder = useMutation<CreateOrder, CreateOrderVariables>(CREATE_ORDER, {
    variables: form.CreateOrderData,
  });

  React.useEffect(() => {
    if(data && data.items) {
      const items = (data.items as GetItems_items[])
      form.seedMenuData(items);
    }
  }, [data])

  React.useEffect(() => {
    if(orderId) {
      window.print();
    }
  }, [orderId]);

  React.useEffect(() => {
    window.onafterprint = () => {
      form.reset();
      setOrderId('');
    }

    return () => { window.onafterprint = null; }
  }, [])

  if (error) {
    return (
      <View style={styles.container}>
        <Text>Error getting menu data: {error.message}</Text>
      </View>
    )
  }

  function onSubmit() {
    createOrder().then(result => {
      const id = result.data && result.data.createOrder.id;
      // @ts-ignore
      setOrderId(id);
    });
  }

  return (
    <>
      <View style={styles.container} testID="no-print">
        <View style={styles.innerContainer}>
          <TextInput
            style={styles.nameInput}
            placeholder="Naam"
            value={form.name}
            onChangeText={form.setName}
          />
          <View style={styles.resetButton}>
            <Button color="red" title="reset" onPress={form.reset} />
          </View>
        </View>
        <View style={styles.innerContainer}>
          <TextInput
            style={styles.remarkInput}
            multiline
            placeholder="Opmerking"
            value={form.remark}
            onChangeText={form.setRemark}
          />
        </View>
        <View style={styles.innerContainer}>
          {form.items.map(item => (
            <NumberInput
              key={item.id}
              value={item.amount}
              label={item.name}
              setValue={item.setAmount}
              min={0}
            />
          ))}
        </View>
        <View style={styles.submitContainer}>
          <Text style={styles.total}>TOTAAL: {form.total}</Text>
          <View style={styles.submitButton}>
            <Button disabled={!form.valid} onPress={onSubmit} title="bestel" />
          </View>
        </View>
      </View>
      <PrintForm id={orderId} />
    </>
  );
})

export default Entry;

const styles = StyleSheet.create({
  container: {
    padding: 10,
  },
  innerContainer: {
    display: "flex",
    flexDirection: "row",
    marginTop: 10,
    flexWrap: "wrap",
  },
  nameInput: {
    padding: 10,
    borderWidth: 1,
    borderStyle: "dashed",
    borderColor: "lightgrey",
    fontSize: 16,
    textAlign: "left",
    marginRight: 10,
  },
  resetButton: {
    justifyContent: "center",
  },
  remarkInput: {
    padding: 10,
    borderWidth: 1,
    borderStyle: "dashed",
    borderColor: "lightgrey",
    fontSize: 16,
    textAlign: "left",
    height: 60,
    textAlignVertical: "top"
  },
  submitContainer: {
    display: "flex",
    flexDirection: 'row',
    justifyContent: "center",
    alignItems: "center"
  },
  total: {
    padding: 10,
    fontSize: 24,
  },
  submitButton: {
    marginRight: 10,
    justifyContent: "center",
  }
});
