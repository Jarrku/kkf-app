import * as React from 'react';
import { View, StyleSheet, TextInput, NativeSyntheticEvent, TextInputKeyPressEventData, Text } from 'react-native';
import { RouteComponentProps } from 'react-router';
import { useMutation } from 'react-apollo-hooks';
import gql from 'graphql-tag';
import { UpdateOrder, UpdateOrderVariables } from '../types';

const UPDATE_ORDER = gql`
  mutation UpdateOrder($data: OrderUpdateInput!, $where: OrderWhereUniqueInput!) {
    updateOrder(data: $data, where: $where) {
      id
      name
    }
  }
`

interface Props extends RouteComponentProps<{ id: string }> { };

const ScanPage = (props: Props) => {
  const [tableNo, setTableNo] = React.useState('');
  const [name, setName] = React.useState('');
  const [error, setError] = React.useState('');

  const id = props.match.params.id;

  const updateOrder = useMutation<UpdateOrder, UpdateOrderVariables>(UPDATE_ORDER, {
    variables: {
      data: { tableNo },
      where: { id }
    }
  });


  const onKeyPress = (event: NativeSyntheticEvent<TextInputKeyPressEventData>) => {
    if (event.nativeEvent.key === 'Enter') {
      updateOrder().then((data) => {
        const name = data.data && data.data.updateOrder && data.data.updateOrder.name

        if (name) {
          setName(name);
        }

        if (data.errors) {
          setError(id);
        }
      }).catch(err => {
        setError(id);
      })
    }
  }

  if (name) {
    return (
      <View style={styles.container}>
        <Text style={styles.success}>Succesvol tafel ingegeven voor: {name}</Text>
      </View>
    )
  }

  if (error) {
    return (<View style={styles.container}>
      <Text style={styles.error}>Geen order ID gevonden in URL ({error})</Text>
    </View>)
  }

  return (
    <View style={styles.container}>
      {id ? (
        <TextInput style={styles.input} onChangeText={setTableNo} value={tableNo} placeholder="Tafelnummer" onKeyPress={onKeyPress}></TextInput>
      ) : (
          <Text style={styles.error}>Geen order ID gevonden in URL ({props.match.params.id})</Text>
        )}
    </View>
  )
}

export default ScanPage;

const styles = StyleSheet.create({
  container: {
    height: '100vh',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  input: {
    textAlign: 'center',
    height: 120,
    padding: 20,
    borderWidth: 1,
    borderStyle: "dashed",
    borderColor: "lightgrey",
  },
  success: {
    textAlign: 'center',
    fontSize: 30,
    height: 120,
    padding: 20,
    color: 'green'
  },
  error: {
    textAlign: 'center',
    fontSize: 30,
    height: 120,
    padding: 20,
    color: 'red'
  }
})