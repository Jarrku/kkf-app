import * as React from 'react';
import { View, StyleSheet, Text, Button } from 'react-native';
import gql from 'graphql-tag';
import { useQuery, useMutation } from 'react-apollo-hooks';
import { GetOrders, GetOrders_orders, OrderSubscription, OrderSubscription_order_node, UpdateOrder, UpdateOrderVariables } from '../types';
import { Subscription } from 'react-apollo';
import kitchenStore from '../models/Kitchen';
import { observer } from 'mobx-react-lite';
import KitchenRow from '../components/KitchenRow';

const GET_ORDERS = gql`
query GetOrders {
  orders {
    id
    name
    remark
    state
    tableNo
    order {
      name
      category
    }
    updatedAt
    scannedAt
    createdAt
  }
}
`;

const ORDER_SUBSCRIPTION = gql`
  subscription OrderSubscription {
    order {
      node {
        id
        name
        remark
        state
        tableNo
        order {
          name
          category
        }
        updatedAt
        scannedAt
        createdAt
      }
    }
  }
`;

const UPDATE_ORDER = gql`
  mutation UpdateOrder($data: OrderUpdateInput!, $where: OrderWhereUniqueInput!) {
    updateOrder(data: $data, where: $where) {
      id
    }
  }
`
class OrderSub extends Subscription<OrderSubscription>{ };

const Kitchen = observer(() => {
  const request = useQuery<GetOrders>(GET_ORDERS);
  const updateOrder = useMutation<UpdateOrder, UpdateOrderVariables>(UPDATE_ORDER);
  const hasSeeded = React.useRef(false);
  const orders = (request.data && request.data.orders as GetOrders_orders[]);
  const [updatedOrder, setUpdatedOrder] = React.useState<OrderSubscription_order_node | undefined>(undefined);

  React.useEffect(() => {
    if (!hasSeeded.current && orders) {
      hasSeeded.current = true;
      kitchenStore.seedOrders(orders);
    }
  }, [orders]);

  React.useEffect(() => {
    if (updatedOrder) {
      kitchenStore.updateOrder(updatedOrder);
    }
  }, [updatedOrder]);

  return (
    <View style={styles.container}>
      <OrderSub subscription={ORDER_SUBSCRIPTION}>
        {({ data }) => {
          const order = data && data.order && data.order.node;
          order && setUpdatedOrder(order);
          return null;
        }}
      </OrderSub>
      <Text style={styles.required}>KIEK NODIG: {kitchenStore.amountOfChicken}</Text>
     <div className="KitchenGrid">
        <Header />
        {kitchenStore.pendingOrders.map(o => (
          <KitchenRow key={o.id} updateOrder={updateOrder} order={o} />
        ))}
      </div>

      <Text>NON SCANNED:</Text>
      {kitchenStore.nonScannedOrders.map(o => (
        <KitchenRow key={o.id} updateOrder={updateOrder} order={o} />
      ))}
    </View>
  );
})

const Header = () => {
  return (
    <div className="KitchenGrid__Row KitchenGrid__Header">
      <span>Gescand</span>
      <span>Naam</span>
      <span>Soep</span>
      <span>Natuur Volw</span>
      <span>Curry Volw</span>
      <span>Provencaal Volw</span>
      <span>Appelmoes Volw</span>
      <span>Natuur Kind</span>
      <span>Curry Kind</span>
      <span>Provencaal Kind</span>
      <span>Appelmoes Kind</span>
      <span>Totaal</span>
      <span>Tafelnr</span>
      <span>Actie</span>
    </div>

  )
}

const HeaderStyles = StyleSheet.create({
  container: {
    display: "flex",
    flexDirection: 'row',
    backgroundColor: "lightgrey",
  }
})

const styles = StyleSheet.create({
  container: {
    height: '100vh',
    display: 'flex',
    padding: 20,
  },
  required: {
    height: 60,
    textAlign: 'center',
    textAlignVertical: 'center',
    fontSize: 20,
    fontWeight: 'bold',
  }
})

export default Kitchen;