Install docker:
- [Mac](https://hub.docker.com/editions/community/docker-ce-desktop-mac)
- [Windows](https://hub.docker.com/editions/community/docker-ce-desktop-windows)

Request static IP from router
Set said IP in `.env` file for the `REACT_APP_API_URL` key.

Run `docker-compose up --build` to spin up application, will be hosted on `${IP}:4000` for other applications on the network.

Press `Ctrl + C` to stop the application

To reset database, run `docker volume ls` and copy the `VOLUME_NAME` values.
For each of these values, run `docker volume rm ${VOLUME_NAME}`