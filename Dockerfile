# build environment
FROM node:10.15.1 as builder
RUN mkdir /usr/src/app
WORKDIR /usr/src/app
ENV PATH /usr/src/app/node_modules/.bin:$PATH

COPY package.json /usr/src/app
COPY yarn.lock /usr/src/app

RUN yarn install --frozen-lockfile

COPY . /usr/src/app
RUN yarn build

# production environment
FROM nginx:1.13.9-alpine
COPY --from=builder /usr/src/app/build /var/www
COPY nginx.conf /etc/nginx/nginx.conf
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
