require('dotenv').config();

const url = `http://${process.env.REACT_APP_API_URL}:${process.env.REACT_APP_API_URL_PORT}`;

module.exports = {
  client: {
    service: {
      name: 'backend',
      url,
      skipSSLValidation: true,
    },
    includes: ['src/**/*.tsx', 'src/**/*.ts'], // load queries from .tsx files
    excludes: ['node_modules/**'] // don't include any matching files from node_modules
  }
};
